-- SQL To Create the database.
create schema IF NOT EXISTS public;
-- Categories table------------------------------------
CREATE TABLE IF NOT EXISTS categories (
  "id" SERIAL PRIMARY KEY ,
  "type" VARCHAR(30) ,
  "color" VARCHAR(20)
);
INSERT INTO categories (type, color)
VALUES  ('Sports', '(0, 85, 255)'),
  ('Geography', '(0, 255, 0)'),
  ('Entertainment', '(255, 0, 0)'),
  ('History', '(237, 245, 16)');
-- Player Table----------------------------------------
CREATE TABLE IF NOT EXISTS players (
  "id" SERIAL PRIMARY KEY ,
  "wins" INT ,
  "name" VARCHAR(30) ,
  "category" INT REFERENCES categories("id")
);
INSERT INTO players (name, wins, category)
VALUES  ('robert', 0, 1),
  ('sayid', 0, 2),
  ('kaan', 0, 3),
  ('mo', 0, 4),
  ('pygame', 6, 4);
-- -- High Score table-------------------------------------
-- CREATE TABLE IF NOT EXISTS high_scores  (
--   "id" SERIAL PRIMARY KEY ,
--   "player" INT REFERENCES "players"("id") ,
--   "category" INT REFERENCES categories("id")
-- );
-- INSERT INTO high_scores (player, category)
-- VALUES  (1, 1),
--   (2, 2),
--   (3, 3),
--   (4, 4);
-- -- Save game table-------------------------------------
-- CREATE TABLE IF NOT EXISTS save_game (
--   "id" SERIAL PRIMARY KEY ,
--   "player" INT REFERENCES "players"("id"),
--   "position_x" INT,
--   "position_y" INT,
--   "category" INT REFERENCES categories("id"),
--   "turn" BOOLEAN,
--   "date" DATE
-- );
