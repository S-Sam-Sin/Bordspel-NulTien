import pygame
import random
import time
import math
import psycopg2


class Database:

    def __init__(self):
        # Connect to an existing database
        self.connection = psycopg2.connect("dbname=postgres user=postgres password= host=localhost")
        # Open a cursor to perform database operations
        self.cursor = self.connection.cursor()

    def select_query(self, query, fetch_all, query_data = None):
        # example to call query data.select_query("""SELECT * FROM players;""")
        self.cursor.execute(query)
        if fetch_all:
            results = self.cursor.fetchall()
        else:
            results = self.cursor.fetchone()
        return results

    def query(self, query, query_data):
        # example to call data.select_query("INSERT INTO test (num, data) VALUES (%s, %s)",(100, "abc'def"))
        self.cursor.execute(query, query_data)
        self.cursor.execute("""COMMIT;""")

    def new_player(self, input_player):
        # Get al users from the database
        all_users = self.select_query("""SELECT * FROM players""", True)
        # get all categories from the database
        all_categories = self.select_query("""SELECT * FROM categories""", True)
        # Loop through all players
        print("-----------------------------------------")
        print(input_player)
        print(turns.currentplayer.category)
        print(all_users)
        print(all_categories)
        time.sleep(1)
        for user in all_users:
            # check if player exists if not create new one
            if user[2] == input_player:
                # Player already exists do not create new data
                main.new_player = True
                break
            else:
                # Player does not exist , new data needs to be created
                main.new_player = False
                # Loop through all categories
                for category in all_categories:
                    # if the users category matches one out of the database
                    if category[1] == turns.currentplayer.category:
                        new_player_cat_id = category[0]
                        break
        if not main.new_player:
            print("inserting query")
            # inserting new player in the database
            self.query("""INSERT INTO players (wins, name, category) VALUES (%s, %s, %s)""", (0, input_player.lower(), new_player_cat_id))

    def player_win(self):
        # put an extra point at the score
        if not main.player_won:
            database_players = data.select_query("""SELECT * FROM players;""", True)
            # Loop through all players
            for player in database_players:
                # if player matches the current player winning break the loop
                if player[2] == turns.currentplayer.name:
                    win_player = player
                    break
            # give the winning player an extra winning point
            win_player_score = win_player[1] + 1
            win_player_name = win_player[2]
            win_player_id = win_player[0]
            if turns.currentplayer.category == "Sports":
                win_player_category = 1
            elif turns.currentplayer.category == "Geography":
                win_player_category = 2
            elif turns.currentplayer.category == "Entertainment":
                win_player_category = 3
            elif turns.currentplayer.category == "History":
                win_player_category = 4
            data.query("""UPDATE players SET wins = (%s), category = (%s) WHERE name = (%s) AND id = (%s)""",
                       (win_player_score, win_player_category, win_player_name, win_player_id))
            main.player_won = True

    def __del__(self):
        # Make the changes to the database
        self.connection.commit()
        # Close communication with the database
        self.cursor.close()
        self.connection.close()


# make some colors
class color:
    def __init__(self):
        self.blue = (0, 85, 255)
        self.black = (0, 0, 0)
        self.white = (255, 255, 255)
        self.red = (255, 0, 0)
        self.green = (0, 255, 0)
        self.yellow = (237, 245, 16)
        self.grey = (128, 128, 128)


# class for the pagenumbers
class page:
    def __init__(self, pagenumber):
        self.pagenumber = pagenumber

    # methods to change the page number
    def setmainpage(self):
        self.pagenumber = 0
        print("mainpage")

    def setinstructionpage(self):
        self.pagenumber = 2
        print("instructionpage")

    def setinstructionpage2(self):
        self.pagenumber = 18
        print("instructionpage2")

    def setinstructionpage3(self):
        self.pagenumber = 17
        print("instructionpage3")

    def setquestionpage(self):
        self.pagenumber = 4
        print("questionpage")

    def setsportquestionpage(self):
        self.pagenumber = 5
        print("sport_questionpage")

    def sethistoryquestionpage(self):
        self.pagenumber = 14
        print("history_questionpage")

    def setentertainmentquestionpage(self):
        self.pagenumber = 13
        print("entertainment_questionpage")

    def setgeographyquestionpage(self):
        self.pagenumber = 15
        print("geography_questionpage")

    def setanswercorrect(self):
        self.pagenumber = 6
        print("answerpage")

    def setanswerincorrect(self):
        self.pagenumber = 7
        print("answerpage")

    def setgamepage(self):
        self.pagenumber = 1
        print("gamepage")

    def start(self):
        self.pagenumber = 11
        print("start")

    def whostarts(self):
        self.pagenumber = 3
        print("whostarts")

    def diceroll(self):
        self.pagenumber = 12
        print("diceroll")

    def diceroll2(self):
        self.pagenumber = 16
        print("extra diceroll")

    def highscore(self):
        self.pagenumber = 40
        print("Highscorepage")

    def check(self):
        if timer.number == 0:
            self.pagenumber = 7
            timer.number = 50
            pygame.mixer.music.load("wrong.mp3")
            pygame.mixer.music.play()


# class for a default font
class Font:

    def __init__(self):
        self.font_dynamix = "./Fonts/Dynamix.ttf"
        self.font_orange_juice = "./Fonts/orangejuice.ttf"
        self.font_skater = "./Fonts/Skater.ttf"
        self.font_doctor_light = "./Fonts/Doctor_Light.ttf"
        self.font_doctor_bold = "./Fonts/Doctor_Bold.ttf"

    def settings(self, filename, size, text, color):
        # Font color RGB in Tulp
        color = color
        # Font position
        # Determinate font size and font file
        # for no font file put the filename value at None
        font = pygame.font.Font(filename, size)
        # put out the string with color
        font_text = font.render(text, 1, color)
        return font_text
Font = Font()


# class with an empty string and a character counter. the values are reset on certain button presses
class Input:
    def __init__(self):
        self.string = ""
        self.characters = 0


# class for buttons With background(clickable)
class button:
    def __init__(self, msg, x, y, w, h, ic, ac, action):
        self.msg = msg
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.ic = ic
        self.ac = ac
        self.action = action
        # get the state of the mouse
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        # check if the state fits the requirements for a buttonpress
        if self.x + self.w > mouse[0] > self.x and self.y + self.h > mouse[1] > y:
            # change colour of the button while mouse on button
            pygame.draw.rect(main.screen, self.ac, (self.x, self.y, self.w, self.h), 3)
            # perform the actions
            if click[0] == 1:
                if self.action == "game":
                    page.setgamepage()
                    time.sleep(1)
                    print(page.pagenumber)
                if self.action == "instruction":
                    page.setinstructionpage()
                    print(page.pagenumber)
                if self.action == "quit":
                    pygame.QUIT()
                if self.action == "start" and gamepage.playerschosen:
                    page.start()
                    names.n = 0
                    time.sleep(1)
                    print(page.pagenumber)
                    whostarts.categories = ["Sports", "Geography", "Entertainment", "History"]
                if self.action == "start" and not gamepage.playerschosen:
                    print("select players please")
                    print(page.pagenumber)
                if self.action == "main":
                    page.setmainpage()
                    main.player_won = False
                    gamepage.buttonstwo = True
                    print(gamepage.buttonstwo)
                    gamepage.buttonsthree = True
                    print(gamepage.buttonsthree)
                    gamepage.buttonsfour = True
                    print(gamepage.buttonsfour)
                    print(page.pagenumber)
                if self.action == "whostarts":
                    page.whostarts()
                    print(page.pagenumber)
                if self.action == "two":
                    gamepage.playerstwo()
                    gamepage.playerschosen = True
                    gamepage.buttonsthree = False
                    gamepage.buttonsfour = False
                    print(gamepage.players)
                if self.action == "three":
                    gamepage.playersthree()
                    gamepage.playerschosen = True
                    gamepage.buttonstwo = False
                    gamepage.buttonsfour = False
                    print(gamepage.players)
                if self.action == "four":
                    gamepage.playersfour()
                    gamepage.playerschosen = True
                    gamepage.buttonstwo = False
                    gamepage.buttonsthree = False
                    print(gamepage.players)
                if self.action == "Sports":
                    turns.currentplayer.category = "Sports"
                    turns.currentplayer.xx = 367
                    turns.currentplayer.yy = 520
                    whostarts.categories.remove("Sports")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "Geography":
                    turns.currentplayer.category = "Geography"
                    turns.currentplayer.xx = 430
                    turns.currentplayer.yy = 520
                    whostarts.categories.remove("Geography")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "Entertainment":
                    turns.currentplayer.category = "Entertainment"
                    turns.currentplayer.xx = 241
                    turns.currentplayer.yy = 520
                    whostarts.categories.remove("Entertainment")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "History":
                    turns.currentplayer.category = "History"
                    turns.currentplayer.xx = 556
                    turns.currentplayer.yy = 520
                    whostarts.categories.remove("History")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "OK":
                    main.stop_loop = False
                    x = turns.currentplayer.category
                    if x == "Sports":
                        page.setsportquestionpage()
                        time.sleep(1)
                    elif x == "Geography":
                        page.setgeographyquestionpage()
                        time.sleep(1)
                    elif x == "Entertainment":
                        page.setentertainmentquestionpage()
                        time.sleep(1)
                    elif x == "History":
                        page.sethistoryquestionpage()
                        time.sleep(1)
                if self.action == "questions":
                    page.setquestionpage()
                if self.action == "correct_answer":
                    pygame.mixer.music.load("./Game_Audio/correct.mp3")
                    pygame.mixer.music.play()
                    page.setanswercorrect()
                    diceroll.update()
                    turns.currentplayer.correct = True
                    timer.number = 50
                    time.sleep(1)
                if self.action == "wrong_answer":
                    pygame.mixer.music.load("./Game_Audio/wrong.mp3")
                    pygame.mixer.music.play()
                    page.setanswerincorrect()
                    timer.number = 50
                    time.sleep(1)
                if self.action == "proceed":
                    print("proceed")
                    print(turns.currentplayer.name)
                    if turns.currentplayer.correct:
                        print(turns.currentplayer.xx, turns.currentplayer.yy)
                        turns.currentplayer.update()
                    print(turns.currentplayer.name)
                    print(turns.currentplayer.xx, turns.currentplayer.yy)
                    print(str(main.extra) + "hello??")
                    if main.extra:
                        page.diceroll2()
                        time.sleep(1)
                    if not main.extra:
                        page.whostarts()
                    time.sleep(1)
                    if turns.currentplayer.won:
                        page.pagenumber = 30
                if self.action == "nextplayer":
                    turns.next()
                    time.sleep(0.2)
                    print(turns.currentplayer.name)
                if self.action == "diceroll":
                    page.diceroll()
                    print(page.pagenumber)
                if self.action == "up":
                    turns.currentplayer.direction = "up"
                    print(turns.currentplayer.direction)
                if self.action == "down":
                    turns.currentplayer.direction = "down"
                    print(turns.currentplayer.direction)
                if self.action == "left":
                    turns.currentplayer.direction = "left"
                    print(turns.currentplayer.direction)
                if self.action == "right":
                    turns.currentplayer.direction = "right"
                    print(turns.currentplayer.direction)
                if self.action == "up2":
                    diceroll2.movingplayer.direction = "up"
                    print(diceroll2.movingplayer.direction)
                if self.action == "down2":
                    diceroll2.movingplayer.direction = "down"
                    print(diceroll2.movingplayer.direction)
                if self.action == "left2":
                    diceroll2.movingplayer.direction = "left"
                    print(diceroll2.movingplayer.direction)
                if self.action == "right2":
                    diceroll2.movingplayer.direction = "right"
                    print(diceroll2.movingplayer.direction)
                if self.action == "proceed2":
                    print("playerdiced")
                    print(diceroll2.movingplayer.name)
                    print(turns.currentplayer.xx, turns.currentplayer.yy)
                    diceroll2.movingplayer.update2()
                    print(turns.currentplayer.xx, turns.currentplayer.yy)
                    main.extra = False
                    page.whostarts()
                if self.action == "update":
                    time.sleep(0.1)
                    self.random = random.randint(1, 4)
                    if self.random == 1:
                        page.pagenumber = 22
                    elif self.random == 2:
                        page.pagenumber = 23
                    elif self.random == 3:
                        page.pagenumber = 21
                    elif self.random == 4:
                        page.pagenumber = 24
                    print(page.pagenumber)
                if self.action == "settings":
                    page.pagenumber = 31
                if self.action == "asknames":
                    page.pagenumber = 32
                    print(page.pagenumber)
                    time.sleep(1)
                if self.action == "highscores":
                    page.highscore()
                if self.action == "sound0":
                    main.sound = True
                    print("sound on")
                if self.action == "sound1":
                    main.sound = False
                    print("sound off")
                if self.action == "soundtest":
                    if main.sound:
                        pygame.mixer.music.load("./Game_Audio/wrong.mp3")
                        pygame.mixer.music.play()
                        time.sleep(1)
                        print("playing sound")
                if self.action == "changeplayer":
                    names.n += 1
                    turns.currentplayer.name = Input.string
                    turns.next()
                    Input.string = ""
                    Input.characters = 0
                    time.sleep(0.2)

        else:
            pass


# class for buttons drawing(clickable)
class button_draw:
    def __init__(self, msg, x, y, w, h, ic, ac, action):
        self.msg = msg
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.ic = ic
        self.ac = ac
        self.action = action
        self.random = 0
        # get the state of the mouse
        mouse = pygame.mouse.get_pos()
        click = pygame.mouse.get_pressed()
        # check if the state fits the requirements for a buttonpress
        if self.x + self.w > mouse[0] > self.x and self.y + self.h > mouse[1] > y:
            # change colour of the button while mouse on button
            pygame.draw.rect(main.screen, self.ic, (self.x, self.y, self.w, self.h))
            pygame.draw.rect(main.screen, self.ac, (self.x, self.y, self.w, self.h), 3)
            # perform the actions
            if click[0] == 1:
                if self.action == "game":
                    page.setgamepage()
                    time.sleep(1)
                    print(page.pagenumber)
                if self.action == "instruction":
                    page.setinstructionpage()
                    print(page.pagenumber)
                if self.action == "quit":
                    pygame.QUIT()
                if self.action == "start" and gamepage.playerschosen:
                    page.start()
                    time.sleep(1)
                    print(page.pagenumber)
                if self.action == "start" and not gamepage.playerschosen:
                    print("select players please")
                    print(page.pagenumber)
                if self.action == "main":
                    page.setmainpage()
                    gamepage.buttonstwo = True
                    gamepage.buttonsthree = True
                    gamepage.buttonsfour = True
                    print(page.pagenumber)
                if self.action == "whostarts":
                    names.n = 0
                    page.whostarts()
                    print(page.pagenumber)
                if self.action == "two":
                    gamepage.playerstwo()
                    gamepage.playerschosen = True
                    gamepage.buttonsthree = False
                    gamepage.buttonsfour = False
                    print(gamepage.players)
                if self.action == "three":
                    gamepage.playersthree()
                    gamepage.playerschosen = True
                    gamepage.buttonstwo = False
                    gamepage.buttonsfour = False
                    print(gamepage.players)
                if self.action == "four":
                    gamepage.playersfour()
                    gamepage.playerschosen = True
                    gamepage.buttonstwo = False
                    gamepage.buttonsthree = False
                    print(gamepage.players)
                if self.action == "Sports":
                    turns.currentplayer.category = "Sports"
                    turns.currentplayer.xx = 367
                    turns.currentplayer.yy = 518
                    whostarts.categories.remove("Sports")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "Geography":
                    turns.currentplayer.category = "Geography"
                    turns.currentplayer.xx = 430
                    turns.currentplayer.yy = 518
                    whostarts.categories.remove("Geography")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "Entertainment":
                    turns.currentplayer.category = "Entertainment"
                    turns.currentplayer.xx = 241
                    turns.currentplayer.yy = 518
                    whostarts.categories.remove("Entertainment")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "History":
                    turns.currentplayer.category = "History"
                    turns.currentplayer.xx = 556
                    turns.currentplayer.yy = 518
                    whostarts.categories.remove("History")
                    whostarts.chosen += 1
                    turns.next()
                    print(whostarts.categories)
                    print(turns.list)
                    print(turns.currentplayer.category)
                    print(turns.currentplayer.name)
                    print(main.listremove)
                if self.action == "OK":
                    main.stop_loop = False
                    x = turns.currentplayer.category
                    if x == "Sports":
                        page.setsportquestionpage()
                        time.sleep(1)
                    elif x == "Geography":
                        page.setgeographyquestionpage()
                        time.sleep(1)
                    elif x == "Entertainment":
                        page.setentertainmentquestionpage()
                        time.sleep(1)
                    elif x == "History":
                        page.sethistoryquestionpage()
                        time.sleep(1)
                if self.action == "cat_sports":
                    page.setsportquestionpage()
                    time.sleep(1)
                if self.action == "questions":
                    page.setquestionpage()
                if self.action == "correct_answer":
                    pygame.mixer.music.load("./Game_Audio/correct.mp3")
                    pygame.mixer.music.play()
                    page.setanswercorrect()
                    diceroll.update()
                    turns.currentplayer.correct = True
                    timer.number = 50
                    time.sleep(1)
                if self.action == "wrong_answer":
                    pygame.mixer.music.load("./Game_Audio/wrong.mp3")
                    pygame.mixer.music.play()
                    page.setanswerincorrect()
                    timer.number = 50
                    time.sleep(1)
                if self.action == "proceed":
                    diceroll.showbuttons = False
                    print("proceed")
                    print(turns.currentplayer.name)
                    print(turns.currentplayer.correct)
                    if turns.currentplayer.correct:
                        print(turns.currentplayer.xx, turns.currentplayer.yy)
                        turns.currentplayer.update()
                        turns.currentplayer.correct = False
                        print(turns.currentplayer.name)
                        print(turns.currentplayer.xx, turns.currentplayer.yy)
                        print(str(main.extra) + "hello??")
                    if main.extra:
                        page.diceroll2()
                        time.sleep(1)
                    if not main.extra:
                        page.whostarts()
                    time.sleep(0.2)
                    if turns.currentplayer.won:
                        page.pagenumber = 30
                if self.action == "nextplayer":
                    turns.next()
                    time.sleep(0.2)
                    print(turns.currentplayer.name)
                if self.action == "diceroll":
                    page.diceroll()
                    print(page.pagenumber)
                if self.action == "up":
                    turns.currentplayer.direction = "up"
                    print(turns.currentplayer.direction)
                if self.action == "down":
                    turns.currentplayer.direction = "down"
                    print(turns.currentplayer.direction)
                if self.action == "left":
                    turns.currentplayer.direction = "left"
                    print(turns.currentplayer.direction)
                if self.action == "right":
                    turns.currentplayer.direction = "right"
                    print(turns.currentplayer.direction)
                if self.action == "up2":
                    diceroll2.movingplayer.direction = "up"
                    print(diceroll2.movingplayer.direction)
                if self.action == "down2":
                    diceroll2.movingplayer.direction = "down"
                    print(diceroll2.movingplayer.direction)
                if self.action == "left2":
                    diceroll2.movingplayer.direction = "left"
                    print(diceroll2.movingplayer.direction)
                if self.action == "right2":
                    diceroll2.movingplayer.direction = "right"
                    print(diceroll2.movingplayer.direction)
                if self.action == "proceed2":
                    print("playerdiced")
                    print(diceroll2.movingplayer.name)
                    print(turns.currentplayer.xx, turns.currentplayer.yy)
                    diceroll2.movingplayer.update()
                    print(turns.currentplayer.xx, turns.currentplayer.yy)
                    main.extra = False
                    page.whostarts()
                if self.action == "update":
                    time.sleep(0.1)
                    self.random = random.randint(1, 4)
                    if self.random == 1:
                        page.pagenumber = 22
                    elif self.random == 2:
                        page.pagenumber = 23
                    elif self.random == 3:
                        page.pagenumber = 21
                    elif self.random == 4:
                        page.pagenumber = 24
                    print(page.pagenumber)
                if self.action == "sound0":
                    main.sound = True
                    print("sound on")
                if self.action == "sound1":
                    main.sound = False
                    print("sound off")
                if self.action == "soundtest":
                    if main.sound:
                        pygame.mixer.music.load("./Game_Audio/wrong.mp3")
                        pygame.mixer.music.play()
                        time.sleep(1)
                        print("playing sound")
                if self.action == "asknames":
                    page.pagenumber = 32
                    print(page.pagenumber)
                    time.sleep(0.2)
                if self.action == "changeplayer":
                    turns.currentplayer.name = Input.string
                    turns.next()
                    data.new_player(Input.string)
                    Input.string = ""
                    Input.characters = 0
                    time.sleep(0.2)

        else:
            pygame.draw.rect(main.screen, self.ic, (self.x, self.y, self.w, self.h))


# create a player class
class player:
    def __init__(self, name, color, x, y, xx, yy, id):
        # single coordinates for name pos, double for image pos
        # newx and newy for the animation destination
        # wins for inserting the value in the database
        # category empty so in whostarts category can be inserted
        # correct to see if answer was correct or not
        # ths decides whether to go to right or wrong answerpage
        # direction is empty, will gain value once pressed on directionbutton in diceroll
        # id for seeing which player we are, so we can check collisions
        # won is to end the game
        # proceed is for if not won: proceed with checkig positions
        self.x = x
        self.xx = xx
        self.yy = yy
        self.y = y
        self.newx = xx
        self.newy = yy
        self.name = name
        self.color = color
        self.wins = 0
        self.category = ""
        self.correct = False
        self.direction = ""
        self.id = id
        self.won = False
        self.proceed = True
        self.skip = True

    def update(self):
        # update is to move the players. newx and newy are set, and in the draw_
        # _we move the players self.xx and self.yy towards the new.xx and new.yy
        print(diceroll.n)
        print(self.direction)
        # check directions and dicerolls and set newx/newy accordingly
        if self.direction == "up":
            if diceroll.n == 1 or diceroll.n == 2:
                self.newy = self.yy - 32
            elif diceroll.n == 3 or diceroll.n == 4:
                self.newy = self.yy - 67
            else:
                self.newy = self.yy - 99
            print("up")
        elif self.direction == "down":
            if diceroll.n == 1 or diceroll.n == 2:
                self.newy = self.yy + 32
            elif diceroll.n == 3 or diceroll.n == 4:
                self.newy = self.yy + 67
            else:
                self.newy = self.yy + 99
            print("down")
        elif self.direction == "left":
            if diceroll.n == 1 or diceroll.n == 2:
                self.newx = self.xx - 63
            elif diceroll.n == 3 or diceroll.n == 4:
                self.newx = self.xx - 125
            else:
                self.newx = self.xx - 190
            print("left")
        elif self.direction == "right":
            if diceroll.n == 1 or diceroll.n == 2:
                self.newx = self.xx + 63
            elif diceroll.n == 3 or diceroll.n == 4:
                self.newx = self.xx + 125
            else:
                self.newx = self.xx + 190
            print("right")

    def update2(self):
        # this update function is for extra dicerolls
        # the players are instantly set to their new position without animation.
        print("been here")
        # check directions and dicerolls and move accordingly
        if diceroll2.movingplayer.direction == "up":
            if diceroll.n == 1 or diceroll.n == 2:
                diceroll2.movingplayer.yy -= 32
                diceroll2.movingplayer.newy = diceroll2.movingplayer.yy
            elif diceroll.n == 3 or diceroll.n == 4:
                diceroll2.movingplayer.yy -= 65
                diceroll2.movingplayer.newy = diceroll2.movingplayer.yy
            else:
                diceroll2.movingplayer.yy -= 99
                diceroll2.movingplayer.newy = diceroll2.movingplayer.yy
            print("up")
        elif diceroll2.movingplayer.direction == "down":
            if diceroll.n == 1 or diceroll.n == 2:
                diceroll2.movingplayer.yy += 32
                diceroll2.movingplayer.newy = diceroll2.movingplayer.yy
            elif diceroll.n == 3 or diceroll.n == 4:
                diceroll2.movingplayer.yy += 65
                diceroll2.movingplayer.newy = diceroll2.movingplayer.yy
            else:
                diceroll2.movingplayer.yy += 99
                diceroll2.movingplayer.newy = diceroll2.movingplayer.yy
            print("down")
        elif diceroll2.movingplayer.direction == "left":
            if diceroll.n == 1 or diceroll.n == 2:
                diceroll2.movingplayer.xx -= 63
                diceroll2.movingplayer.newx = diceroll2.movingplayer.xx
            elif diceroll.n == 3 or diceroll.n == 4:
                diceroll2.movingplayer.xx -= 125
                diceroll2.movingplayer.newx = diceroll2.movingplayer.xx
            else:
                diceroll2.movingplayer.xx -= 190
                diceroll2.movingplayer.newx = diceroll2.movingplayer.xx
            print("left")
        elif diceroll2.movingplayer.direction == "right":
            if diceroll.n == 1 or diceroll.n == 2:
                diceroll2.movingplayer.xx += 63
                diceroll2.movingplayer.newx = diceroll2.movingplayer.xx
            elif diceroll.n == 3 or diceroll.n == 4:
                diceroll2.movingplayer.xx += 125
                diceroll2.movingplayer.newx = diceroll2.movingplayer.xx
            else:
                diceroll2.movingplayer.xx += 190
                diceroll2.movingplayer.newx = diceroll2.movingplayer.xx
            print("right")

    def check(self):
        # this checks for any movement outside the field or any collisions with other players.
        # if in the lower box, check for current position. if position changes into upper box
        # adjust the self.xx accordingly. now we set self.skip False so we wont check this anymore
        # if we move back to the lower box, self.skip = True
        if self.yy > 205:
            self.skip = True
        # check if positions did not pass the borders
        # here we check for the winning state
        # if we won self.proceed is false. we will not check anything else anymore
        if self.direction == "up":
            # check if player won
            if self.yy < 38:
                self.xx = 400
                self.newx = 400
                self.newy = 20
                self.yy = 20
                print("you won")
                self.won = True
                print(self.won)
                pygame.mixer.music.load("./Game_Audio/victory.mp3")
                pygame.mixer.music.play()
                time.sleep(1)
                page.pagenumber = 30
                self.proceed = False
            if self.proceed:
                if self.skip:
                    # 1st 2nd 7th 8th row check if too high, adjusted accordingly
                    # after adjustment skip = False, so we will no longer check this block.
                    # until we move down to lower block, then skip = True
                    # (skip is a poorly chosen name, we dont skip when True and skip when False)
                    if 150 < self.xx < 270:
                        if 100 < self.yy < 205:
                            print("corrected self.xx, newx(left side)")
                            self.xx = 305
                            self.newx = 305
                            self.skip = False
                    elif 530 < self.xx < 650:
                        if 50 < self.yy < 205:
                            self.xx = 495
                            self.newx = 495
                            print("corrected self.xx,newx(right side)")
                            self.skip = False
                    # middle bottown block check if at middle border
                    # if we are at the middle borders, we check if we go into the upper block.
                    # skip same as above
                    elif 270 < self.xx < 400:
                        if 50 < self.yy < 205:
                            self.xx = 368
                            self.newx = 368
                            print("corrected middle self.xx,newx(blue)")
                            self.skip = False
                    elif 400 < self.xx < 530:
                        if 50 < self.yy < 205:
                            self.xx = 430
                            self.newx = 430
                            print("corrected middle self.xx,newx(green)")
                            self.skip = False
        if self.proceed:
            # this is for the lower block
            if self.direction == "left":
                # left row check if not too far
                # if too far reset self.xx and self.newx
                if self.xx < 150:
                    self.xx = 178
                    self.newx = 178
                    print("corrected left")
            if self.direction == "right":
                # right row check if not too far
                # if too far reset self.xx and self.newx
                if self.xx > 650:
                    self.xx = 620
                    self.newx = 620
                    print("corrected right")
            # this is for the upper block
            if self.direction == "right" or self.direction == "left":
                # 3rd 4th 5th 6th row if not too far left/right
                # if too far, reset xpos
                if 200 > self.yy > 38:
                    if self.xx < 275:
                        self.xx = 300
                        self.newx = 300
                    if self.xx > 525:
                        self.xx = 495
                        self.newx = 495
                        print("corrected mid left/right")
            # this is for lower border
            if self.direction == "down":
                if self.yy > 530:
                    self.yy = 518
                    self.newy = 518
                    print("corrected downline")
            # set the self.correct False so it doesnt stay True when going into next question
            self.correct = False
            # check postition and change category accordingly
            if self.yy > 205:
                if 150 < self.xx < 270:
                    self.category = "Entertainment"
                elif 270 < self.xx < 400:
                    self.category = "Sports"
                elif 400 < self.xx < 525:
                    self.category = "Geography"
                elif 525 < self.xx < 650:
                    self.category = "History"
            elif self.yy < 205:
                if 270 < self.xx < 335:
                    self.category = "Entertainment"
                elif 335 < self.xx < 400:
                    self.category = "Sports"
                elif 400 < self.xx < 460:
                    self.category = "Geography"
                elif 460 < self.xx < 525:
                    self.category = "History"
            # check for collisions with other players
            # if player1 just moved:
            # check if he had collisions with any other player
            # if so, send that player to diceroll2 and let him move.
            if self.id == 1:
                print("my id is 1")
                # if player 2 has a collision with player1
                if self.xx - 10 < player2.xx < self.xx + 10 and \
                                                self.yy - 10 < player2.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player2
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    print(page.pagenumber)
                    print("page = diceroll2?")
                    # set extra to True
                    main.extra = True
                # if player 3 has a collision with player1
                elif self.xx - 10 < player3.xx < self.xx + 10 and \
                                                self.yy - 10 < player3.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player3
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    print(page.pagenumber)
                    print("page = diceroll2?")
                    main.extra = True
                # if player 4 has a collision with player1
                elif self.xx - 10 < player4.xx < self.xx + 10 and \
                                                self.yy - 10 < player4.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player4
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    print(page.pagenumber)
                    main.extra = True
                    print("page = diceroll2?")
            # if player2 just moved
            elif self.id == 2:
                # if player 1 has a collision with player1
                if self.xx - 10 < player1.xx < self.xx + 10 and \
                                                self.yy - 10 < player1.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player1
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
                # if player 3 has a collision with player1
                elif self.xx - 10 < player3.xx < self.xx + 10 and \
                                                self.yy - 10 < player3.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player3
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
                # if player 4 has a collision with player1
                elif self.xx - 10 < player4.xx < self.xx + 10 and \
                                                self.yy - 10 < player4.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player4
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
            elif self.id == 3:
                # if player 1 has a collision with player1
                if self.xx - 10 < player1.xx < self.xx + 10 and \
                                                self.yy - 10 < player1.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player1
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
                # if player 3 has a collision with player1
                elif self.xx - 10 < player2.xx < self.xx + 10 and \
                                                self.yy - 10 < player2.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player2
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
                # if player 4 has a collision with player1
                elif self.xx - 10 < player4.xx < self.xx + 10 and \
                                                self.yy - 10 < player4.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player4
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
            elif self.id == 4:
                # if player 1 has a collision with player1
                if self.xx - 10 < player1.xx < self.xx + 10 and \
                                                self.yy - 10 < player1.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player1
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
                # if player 3 has a collision with player1
                elif self.xx - 10 < player3.xx < self.xx + 10 and \
                                                self.yy - 10 < player3.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player3
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")
                # if player 4 has a collision with player1
                elif self.xx - 10 < player2.xx < self.xx + 10 and \
                                                self.yy - 10 < player2.yy < self.yy + 10:
                    print("ive been here")
                    # the diceroll 2 page movingplayer is set to the player that was collided with
                    diceroll2.movingplayer = player2
                    print(diceroll2.movingplayer.name)
                    # the diceroll 2 page is updated
                    diceroll2.update()
                    print("updated")
                    # the diceroll 2 page is drawn
                    page.diceroll2()
                    main.extra = True
                    print(page.pagenumber)
                    print("page = diceroll2?")

    def draw(self, screen):
        # This is the playername and playerscore in the sidebar of the game
        playername = Font.settings(None, 30, self.name, self.color)
        playerscore = Font.settings(None, 30, str(self.wins), color.black)

        # These are the players being drawn. they move at 60pixels per second.
        # they stop moving when they reached their newx/newy
        # after that they check if they went out of the map, or had a collision
        # then the buttons are drawn so you can go to the next question/player
        if self.direction == "up":
            if self.yy > self.newy:
                self.yy -= 1
                time.sleep(0.01)
                startgame.drawbuttons = False
            elif self.yy == self.newy:
                startgame.drawbuttons = True
                self.check()
                print("checked")
        if self.direction == "down":
            if self.yy < self.newy:
                self.yy += 1
                time.sleep(0.01)
                startgame.drawbuttons = False
            elif self.yy == self.newy:
                startgame.drawbuttons = True
                self.check()
                print("checked")
        if self.direction == "left":
            if self.xx > self.newx:
                self.xx -= 1
                time.sleep(0.01)
                startgame.drawbuttons = False
                self.check()
            elif self.xx == self.newx:
                startgame.drawbuttons = True
        if self.direction == "right":
            if self.xx < self.newx:
                self.xx += 1
                time.sleep(0.01)
                startgame.drawbuttons = False
                self.check()
            elif self.xx == self.newx:
                startgame.drawbuttons = True
        # actually draw the players
        pygame.draw.circle(screen, self.color, (self.xx, self.yy), 10)

        # actually draw the playername/score
        screen.blit(playername, (self.x, self.y))
        screen.blit(playerscore, (self.x, (self.y + 20)))


# class for the default screen
class mainpage:
    def draw(self, screen):
        # draw background
        startimg = pygame.image.load("./Design_Afbeeldingen/startscherm.png")
        screen.blit(startimg, (0, 0))

        # draw start button, if pressed move to the next screen
        button("start", 269, 158, 265, 72, None, color.green, "game")
        start_button_font = Font.settings(None, 40, "", color.black)

        # draw quit button, if pressed quit the game
        button("quit", 269, 485, 265, 72, None, color.green, "quit")
        quit_button_font = Font.settings(None, 40, "", color.black)

        # draw instructions button, if pressed move to instructionpage
        button("instructions", 269, 321, 265, 72, None, color.green, "instruction")
        inststurctions_button_font = Font.settings(None, 40, "", color.black)

        # draw Highscores button, if pressed go to highscore page
        button("highscores", 269, 239, 265, 72, None, color.green, "highscores")

        # draw settings button, if pressed move to settings page
        button("settings", 269, 403, 265, 72, None, color.green, "settings")

        # blit the button fonts.
        screen.blit(start_button_font, (370, 247))
        screen.blit(quit_button_font, (370, 430))
        screen.blit(inststurctions_button_font, (370, 340))


# class for the instruction screen
class instructionpage:
    def draw(self, screen):
        # draw the text of the instructions( in an image because of pygame text reasons)
        instructionimg = pygame.image.load("./Design_Afbeeldingen/instructions.png")
        screen.blit(instructionimg, (0, 0))

        # draw the back button, if pressed go back to start page
        button("back", 16, 545, 90, 40, color.green, color.green, "main")


class timer:
    def __init__(self):
        # self.number = 50 so the timer starts at 50
        # self.sec = 0 and counts up every tick of the game(15 per sec)
        # if sec % 15 == 0(this is every 1 in 15 tick so once a second)
        # count the timer down
        self.number = 50
        self.tick = 0

    def update(self):
        self.tick += 1
        if self.tick % 15 == 0:
            self.number -= 1
timer = timer()


class Questionpage:

    def __init__(self, screen):
        # self.categories = ["Sports"]
        self.categories = ["Sports", "Geography", "Entertainment", "History"]
        self.choose = random.choice(self.categories)
        # Create empty list.
        self.answers = []
        self.action = []
        self.button_list = []
        self.screen = screen
        # Put an random int in variable so it will random pick an question.
        self.pick_question = int(random.randint(0, 0))
        self.question = ""
        self.question_font = None
        self.correct_answer = None
        self.answer_button_font_1 = ""
        self.answer_button_font_2 = ""
        self.answer_button_font_3 = ""
        self.number = random.randint(0, 15)

    def draw_category(self, screen):
        # set the background
        your_category_BG = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(your_category_BG, (0, 0))

        # create question font
        question = "Your category is: " + turns.currentplayer.category
        question_font = Font.settings(Font.font_doctor_bold, 45, question, color.black)

        # Create sports button
        button_draw("OK", 318, 263, 160, 72, color.grey, color.green, "OK")
        ok_font = Font.settings(Font.font_orange_juice, 40, "OK", color.black)

        # blit that shit
        screen.blit(question_font, (150, 150))
        screen.blit(ok_font, (375, 285))

    def draw_questions(self, category, color_category, question, ans_1, ans_2, ans_3):
        category = category
        categorie_font = Font.settings(Font.font_skater, 40, category, color_category)
        if not main.stop_loop:
            main.stop_loop = True
            # Create question.
            self.question = question
            self.question_line1 = self.question[0]
            self.question_line2 = self.question[1]
            self.question_font1 = Font.settings(Font.font_doctor_light, 33, self.question_line1, color.black)
            self.question_font2 = Font.settings(Font.font_doctor_light, 33, self.question_line2, color.black)
            # Double check if there are two answers
            # Create a list with answer for the loop in buttons and make it random order
            if ans_3[0] == "" and ans_3[1] == "":
                self.button_list = {0: (ans_1[0], ans_1[1]),
                                    1: (ans_2[0], ans_2[1])}
            else:
                self.button_list = {0: (ans_1[0], ans_1[1]),
                                    1: (ans_2[0], ans_2[1]),
                                    2: (ans_3[0], ans_3[1])}
            random.shuffle(self.button_list)
        how_many = len(self.button_list)

        # Create buttons for the answers
        if how_many == 3:
            button_draw(self.button_list[0][0], 150, 240, 500, 72, color.grey, color.green, self.button_list[0][1])
            self.answer_button_font_1 = Font.settings(None, 30, "A. " + self.button_list[0][0], color.white)

            button_draw(self.button_list[1][0], 150, 340, 500, 72, color.grey, color.green, self.button_list[1][1])
            self.answer_button_font_2 = Font.settings(None, 30, "B. " + self.button_list[1][0], color.white)

            button_draw(self.button_list[2][0], 150, 440, 500, 72, color.grey, color.green, self.button_list[2][1])
            self.answer_button_font_3 = Font.settings(None, 30, "C. " + self.button_list[2][0], color.white)
        elif how_many == 2:
            button_draw(self.button_list[0][0], 150, 240, 500, 72, color.grey, color.green, self.button_list[0][1])
            self.answer_button_font_1 = Font.settings(None, 30, "A. " + self.button_list[0][0], color.white)

            button_draw(self.button_list[1][0], 150, 340, 500, 72, color.grey, color.green, self.button_list[1][1])
            self.answer_button_font_2 = Font.settings(None, 30, "B. " + self.button_list[1][0], color.white)
        # blit that shit
        self.screen.blit(self.question_font1, (30, 110))
        self.screen.blit(self.question_font2, (30, 150))
        self.screen.blit(categorie_font, (330, 50))
        self.screen.blit(self.answer_button_font_1, (160, 270))
        self.screen.blit(self.answer_button_font_2, (160, 370))
        # print the timer on the screen
        timerstr = Font.settings(None, 30, str(timer.number), color.white)
        self.screen.blit(timerstr, (10, 10))
        timer.update()
        if how_many == 3:
            self.screen.blit(self.answer_button_font_3, (160, 470))
        pygame.display.update()
        # check if the time has run out
        page.check()

    def draw_sports(self, screen):
        questions_sports_BG = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(questions_sports_BG, (0, 0))
        # Put all the questions in place
        questions = [
            [("Welke manier van sport word ", "het meest beoefend in Rotterdam?"),
             ("Fitness", "correct_answer"),
             ("Voetbal", "wrong_answer"),
             ("Basketball", "wrong_answer")],
            [("Hoe heet het centrum voor sport naast de kuip?", ""),
             ("Schuttersveld", "wrong_answer"),
             ("Topsportcentrum Rotterdam", "correct_answer"),
             ("Sportcentrum de Wilgenring", "wrong_answer")],
            [("In welk jaar startte de Tour de France in Rotterdam?", ""),
             ("2000", "wrong_answer"),
             ("2005", "wrong_answer"),
             ("2010", "correct_answer")],
            [("Welk tennistoernooi word er elk jaar in Ahoy gehouden?", ""),
             ("ABN AMRO World Tennis Tournament", "correct_answer"),
             ("Ahoy Open", "wrong_answer"),
             ("Heineken Open", "wrong_answer")],
            [("Wat is een hockeyclub uit Rotterdam?", ""),
             ("HVGR", "wrong_answer"),
             ("Focus", "wrong_answer"),
             ("HC Rotterdam", "correct_answer")],
            [("Welke Olympiër groeide op in Rotterdam?", ""),
             ("Dorian van Rijsselberghe", "wrong_answer"),
             ("Marhinde Verkerk", "correct_answer"),
             ("Edith Bosch", "wrong_answer")],
            [("Op welke baan vond het WK roeien in 2016 plaats?", ""),
             ("Willem Alexander baan", "correct_answer"),
             ("Beatrix baan", "wrong_answer"),
             ("Juliana baan", "wrong_answer")],
            [("Voor welke 3 sporten is de", "Willem Alexander baan het meest geschikt?"),
             ("Watersporten, Wielrennen en hardlopen", "correct_answer"),
             ("Voetbal, Hockey en basketbal", "wrong_answer"),
             ("Fitness, hardlopen en basketbal", "wrong_answer")],
            [("Op welke positie in het veld speelde", "Coen Moulijn voor zowel Feyenoord als het Nederlands elftal?"),
             ("Rechtsback", "wrong_answer"),
             ("Linksback", "wrong_answer"),
             ("Linksbuiten", "correct_answer")],
            [("Hoe heet het stadion van Sparta Rotterdam?", ""),
             ("De Toren", "wrong_answer"),
             ("Het Kasteel", "correct_answer"),
             ("De Arena", "wrong_answer")],
            [("Hoe lang is de NN Marathon van Rotterdam?", ""),
             ("42,125 km", "correct_answer"),
             ("42,450 km", "wrong_answer"),
             ("42,680 km", "wrong_answer")],
            [("Hoeveel spelers staan er", "per team bij lacrosse op het veld?"),
             ("9", "wrong_answer"),
             ("10", "correct_answer"),
             ("11", "wrong_answer")],
            [("In welk jaar is de honkbalclub Neptunes opgericht?", ""),
             ("1850", "wrong_answer"),
             ("1875", "wrong_answer"),
             ("1900", "correct_answer")],
            [("Een honkbal is groter dan een softbal.", ""),
             ("Waar", "wrong_answer"),
             ("Niet Waar", "correct_answer"),
             ("Even Groot", "wrong_answer")],
            [("Hoeveel mensen staan er", "achter de slagman bij honkbal?"),
             ("1", "wrong_answer"),
             ("2", "correct_answer"),
             ("3", "wrong_answer")]
        ]
        # call in the questions
        random.shuffle(questions)
        if len(questions[0]) == 4:
            self.draw_questions("Sports", color.blue,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                questions[0][3])
        else:
            self.draw_questions("Sports", color.blue,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                ("", ""))

    def draw_geography(self, screen):
        questions_geography_BG = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(questions_geography_BG, (0, 0))
        questions = [
            [("Welke brug in Rotterdam heeft de volgende bijnaam: De zwaan.", ""),
             ("De Willemsbrug", "wrong_answer"),
             ("De Erasmusbrug", "correct_answer"),
             ("De van Briennenoordbrug", "wrong_answer")],
            [("Rotterdam is de hoofdstad van Nederland.", ""),
             ("Waar", "wrong_answer"),
             ("Niet Waar", "correct_answer")],
            [("Rotterdam is de hoofdstad van Zuid-Holland.", ""),
             ("Waar", ""),
             ("Niet Waar", "correct_answer")],
            [("Rotterdam is de grootste stad van Nederland.", ""),
             ("Waar", "correct_answer"),
             ("Niet Waar", "wrong_answer")],
            [("De haven van Rotterdam is de grootste haven van Nederland.", ""),
             ("Waar", "correct_answer"),
             ("Niet Waar", "wrong_answer")],
            [("Wat is het belangrijkste vervoersmiddel in Rotterdam?.", ""),
             ("Metro", "correct_answer"),
             ("Auto", "wrong_answer"),
             ("Fiets", "wrong_answer")],
            [("Hoeveel millimeter regen valt er", "gemiddeld per jaar in Rotterdam?"),
             ("760 tot 780mm", "wrong_answer"),
             ("780 tot 800mm", "wrong_answer"),
             ("800 tot 820mm", "correct_answer")],
            [("Hoeveel woningen zijn er ongeveer in Rotterdam?", ""),
             ("150.000", "wrong_answer"),
             ("300.000", "wrong_answer"),
             ("450.000", "correct_answer")],
            [("Wat is het oudste gebouw van Rotterdam?.", ""),
             ("Kerktoren hillegondakerk", "correct_answer"),
             ("St. Laurenskerk.", "wrong_answer"),
             ("Stadhuis van Rotterdam", "wrong_answer")],
            [("Hoeveel mensen maken dagelijks gebruik", "van het openbaar vervoer in Rotterdam?"),
             ("800.000", "wrong_answer"),
             ("900.000", "wrong_answer"),
             ("1.000.000", "correct_answer")],
            [("Wat is de oudste brug van Rotterdam?", ""),
             ("De Willemsbrug", "wrong_answer"),
             ("De Koninginnebrug", "correct_answer"),
             ("De van Briennenoordbrug", "wrong_answer")],
            [("Rotterdam word ook wel de …. Genoemd", ""),
             ("stad der wondereng", "wrong_answer"),
             ("stad der steden", "wrong_answer"),
             ("Haven stad", "correct_answer")],
            [("In welke provincie ligt Rotterdam?", ""),
             ("Noord-Holland", "wrong_answer"),
             ("Zuid-Holland", "correct_answer"),
             ("Noord-Brabant", "wrong_answer")],
            [("Hoe heet de grootste rivier waar Rotterdam aan grenst?", ""),
             ("De Maas", "correct_answer"),
             ("De Rijn", "wrong_answer"),
             ("De Waal", "wrong_answer")],
            [("De afstand tussen Rotterdam is ongeveer?", ""),
             ("50 tot 60km", "wrong_answer"),
             ("60 tot 70km", "wrong_answer"),
             ("70 tot 80km", "correct_answer")],
        ]
        random.shuffle(questions)
        # call in the questions
        if len(questions[0]) == 4:
            self.draw_questions("Geography", color.green,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                questions[0][3])
        else:
            self.draw_questions("Geography", color.green,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                ("", ""))

    def draw_entertainment(self, screen):
        questions_entertainment_BG = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(questions_entertainment_BG, (0, 0))

        questions = [
            [("Welke bar in Rotterdam werd in", "2009 de beste bar ter wereld benoemd?"),
             ("De Witte Aap", "correct_answer"),
             ("Het NRC", "wrong_answer"),
             ("Café de Beurs", "wrong_answer")],
            [("Hoe heet de bekendste escape room in Rotterdam?", ""),
             ("R’dam Escape", "wrong_answer"),
             ("Escape010", "correct_answer"),
             ("Room Escape", "wrong_answer")],
            [("Voor welk vervoermiddel is er", "geen tour door Rotterdam beschikbaar?"),
             ("Segway", "wrong_answer"),
             ("Boot", "wrong_answer"),
             ("Auto", "correct_answer")],
            [("Welk van de volgende winkels is niet rond de koopgoot?", ""),
             ("H&M", "wrong_answer"),
             ("Media Markt", "correct_answer"),
             ("The Sting", "wrong_answer")],
            [("In welke bioscoop vindt het Wildlife Film Festival plaats?", ""),
             ("Cinerama", "correct_answer"),
             ("Pathé de Kuip", "wrong_answer"),
             ("Pathé Schouwburgplein", "wrong_answer")],
            [("Voor welk museum staat het monument", "van Zadkine genaamd “De Verwoest Stad”?"),
             ("Havenmuseum", "wrong_answer"),
             ("Mariniersmuseum", "wrong_answer"),
             ("Maritiem museum", "correct_answer")],
            [("Waar geeft de Rotterdam Tours onder andere rondleidingen?", ""),
             ("De Euromast", "wrong_answer"),
             ("Museumplein", "wrong_answer"),
             ("De Markthal", "correct_answer")],
            [("Welke van de volgende Pathé bioscopen is niet in Rotterdam?", ""),
             ("Pathé de Kuip", "wrong_answer"),
             ("Pathé de Kroon", "correct_answer"),
             ("Pathé Schouwburgplein", "wrong_answer")],
            [("Hoeveel bezoekers zijn er jaarlijks bij de Marathon Rotterdam?", ""),
             ("925.000 bezoekers", "correct_answer"),
             ("675.000 bezoekers", "wrong_answer"),
             ("830.000 bezoekers", "wrong_answer")],
            [("Waar kan je niet terecht om te gaan zwemmen?", ""),
             ("Hoek van Holland", "wrong_answer"),
             ("Euromast Park", "correct_answer"),
             ("Plaswijckpark", "wrong_answer")],
            [("Welke landen kun je behalve", "Nederland ook in Miniworld Rotterdam zien?"),
             ("Luxemburg en België", "correct_answer"),
             ("Duitsland en België", "wrong_answer"),
             ("Duitsland en Frankrijk", "wrong_answer")],
            [("Hoe heet de culturele en culinaire", "ontdekkingstocht door Rotterdam?"),
             ("Drive & Eat", "wrong_answer"),
             ("Bicycle Diner", "wrong_answer"),
             ("Bike & Bite", "correct_answer")],
            [("Welk van de volgende restaurantboten", "in Rotterdam bestaat niet?"),
             ("De Zwanenboot", "correct_answer"),
             ("De Pannenkoekenboot", "wrong_answer"),
             ("De Berenboot", "wrong_answer")],
            [("Welk van de volgende bioscopen is het oudst?", ""),
             ("Cinerama", "wrong_answer"),
             ("Pathé de Kuip", "wrong_answer"),
             ("LantarenVenster", "correct_answer")],
            [("Op welk plein vindt jaarlijkse het Najaarskermis Rotterdam plaats?", ""),
             ("Mullerpier", "correct_answer"),
             ("Pier 80", "wrong_answer"),
             ("Schouwburgplein", "wrong_answer")],
        ]
        random.shuffle(questions)
        # call in the questions
        if len(questions[0]) == 4:
            self.draw_questions("Entertainment", color.red,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                questions[0][3])
        else:
            self.draw_questions("Entertainment", color.red,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                ("", ""))

    def draw_history(self, screen):
        questions_history_BG = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(questions_history_BG, (0, 0))

        # Put all the questions in place
        questions = [
            [("Waar dankt Rotterdam zijn naam aan?", ""),
             ("Kooplieden hadden dit vroeger bedacht", "wrong_answer"),
             ("Aan de rivier de rotte", "correct_answer"),
             ("Er was een dam aangelegd in de maas", "wrong_answer")],
            [("Wat is het enigste overgebleven", "middeleeuws gebouw in de binnenstad van Rotterdam?"),
             ("De oude haven", "wrong_answer"),
             ("VOC magazijn Rotterdam", "wrong_answer"),
             ("St. Laurenskerk", "correct_answer")],
            [("Wie is de nachtburgemeester van Rotterdam?", ""),
             ("Ahmed Aboutaleb", "wrong_answer"),
             ("Jules Deelder", "correct_answer"),
             ("Willem Alexander", "wrong_answer")],
            [("Was de eerste metrolijn in Nederland in Rotterdam geopend?", ""),
             ("Waar, in 1968", "correct_answer"),
             ("Niet waar", "wrong_answer")],
            [("Waar stond vroeger de wijk Katendrecht om bekend?", ""),
             ("De beste bakker van de stad was daar gevestigd", "wrong_answer"),
             ("De prostituees", "correct_answer"),
             ("De oudste beschermde boom van de stad staat daar", "wrong_answer")],
            [("Wanneer is diergaarde Blijdorp geopend?", ""),
             ("1855", "correct_answer"),
             ("1975", "wrong_answer"),
             ("1995", "wrong_answer")],
            [("Wat is de officiële naam van de koopgoot?", ""),
             ("De ondergrondse winkelstraat", "wrong_answer"),
             ("Beurstraverse", "correct_answer"),
             ("Gewoon de koopgoot", "wrong_answer")],
            [("Welk gebouw (gebouwd in 1957)", "stond symbool voor de wederopbouw van de stad?"),
             ("De Bijenkorf", "correct_answer"),
             ("De Kubuswoningen", "wrong_answer"),
             ("The red apple", "wrong_answer")],
            [("Rotterdam voor de Tweede Wereldoorlog?", ""),
             ("ca. 5000", "wrong_answer"),
             ("ca. 8000", "wrong_answer"),
             ("ca. 12000", "correct_answer")],
            [("Wereldoorlog de enige weg naar het", "centrum die de Duitsers probeerden te bereiken?"),
             ("De nieuwe Binnenweg", "wrong_answer"),
             ("Maasbrug", "correct_answer"),
             ("Koninginnenbrug", "wrong_answer")],
            [("Rotterdam was tot 1870 een opslag haven,", "welke producten werden er onder ander opgeslagen?"),
             ("Suiker", "correct_answer"),
             ("Wol", "correct_answer"),
             ("Cacao", "wrong_answer")],
            [("Door welke architect(en) is de Euromast ontworpen?", ""),
             ("Maaskant", "correct_answer"),
             ("Brinkman en van der Vlugt", "wrong_answer"),
             ("Koolhaas", "wrong_answer")],
            [("In welk jaar heeft Rotterdam stadsrechten gekregen?", ""),
             ("1250", "wrong_answer"),
             ("1340 en van der Vlugt", "correct_answer"),
             ("1590", "wrong_answer")],
            [("Hoe heette de haven van Rotterdam", "oorspronkelijk tijdens zijn ontstaan?"),
             ("Waalhaven", "correct_answer"),
             ("De Maashaven", "wrong_answer"),
             ("Europoort", "wrong_answer")],
        ]
        random.shuffle(questions)
        # call in the questions
        if len(questions[0]) == 4:
            self.draw_questions("History", color.yellow,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                questions[0][3])
        else:
            self.draw_questions("History", color.yellow,
                                questions[0][0],
                                questions[0][1],
                                questions[0][2],
                                ("", ""))

    def draw_rating(self, correct, screen):
        rating_BG = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(rating_BG, (0, 0))
        # Answer is correct
        rating = "Your answer is "
        if correct == 1:
            rating += "CORRECT"
            color_answer = color.green
            correct_img = pygame.image.load("./Design_Afbeeldingen/correct1.png")
            screen.blit(correct_img, (0, 0))
        # Answer is incorrect
        elif correct == 0:
            rating += "INCORRECT"
            color_answer = color.red
            incorrect_img = pygame.image.load("./Design_Afbeeldingen/incorrect.png")
            screen.blit(incorrect_img, (0, 0))

        question_font = Font.settings(Font.font_skater, 45, rating, color_answer)

        # Create button to proceed
        if correct == 1:
            button_draw("proceed", 318, 263, 144, 72, color.grey, color.green, "diceroll")
            proceed_font = Font.settings(Font.font_doctor_bold, 35, "proceed", color.black)
        else:
            button_draw("proceed", 318, 263, 144, 72, color.grey, color.green, "proceed")
            proceed_font = Font.settings(Font.font_doctor_bold, 35, "proceed", color.black)

        # blit that shit
        screen.blit(question_font, (175, 150))
        screen.blit(proceed_font, (342, 279))


class HighScore:

    def draw(self, screen):
        #page background
        highscores_BG = pygame.image.load("./Design_Afbeeldingen/highscores.png")
        screen.blit(highscores_BG, (0, 0))

        results = data.select_query("""SELECT name, wins, type AS category, color
                                    FROM players
                                    INNER JOIN categories
                                    ON players.category = categories.id ORDER BY wins DESC LIMIT 5;""", True)
        # count how many results
        how_many = len(results)
        # begin position of the results in Y
        position_y = 185
        # begin font size
        font_size = 40
        # Loop the results from the database
        for i in range(how_many):
            player = str(results[i][0])
            player_font = Font.settings(None, font_size, player, color.black)
            player_score = str(results[i][1])
            player_score_font = Font.settings(None, font_size, player_score, color.black)
            player_category = str(results[i][2])
            player_category_font = Font.settings(None, font_size, player_category, color.black)
            screen.blit(player_font, (48, position_y))
            screen.blit(player_score_font, (313, position_y))
            screen.blit(player_category_font, (480, position_y))
            # the new y position for the next result
            position_y += 68
            # the lower the position , the lower the font size
            font_size -= 3

        # back Button
        button("back", 16, 543, 90, 40, color.red, color.green, "main")

        # Titles
        title = Font.settings(None, 50, "", color.black)
        name = Font.settings(None, 35, "", color.black)
        scores = Font.settings(None, 35, "", color.black)
        category = Font.settings(None, 35, "", color.black)

        # # blit that shit
        screen.blit(title, (250, 50))
        screen.blit(name, (30, 150))
        screen.blit(scores, (310, 150))
        screen.blit(category, (510, 150))


# class for the game screen
class gamepage:
    def __init__(self):
        # self.players = 0 so we can store a player amount in it
        # self.playerschosen = False, will be True if a number is picked
        # then the continue button will be useable
        # other value are for showing buttons when one is pressed or not
        # if 2 is pressed, show only 2.. etc.
        self.players = 0
        self.playerschosen = False
        self.buttonstwo = True
        self.buttonsthree = True
        self.buttonsfour = True

    def draw(self, screen):
        # background playerselection
        playerselection = pygame.image.load("./Design_Afbeeldingen/playerselection.png")
        screen.blit(playerselection, (0, 0))

        # ask how many players
        question = Font.settings(None, 30, "", color.white)
        # quit button
        button("back", 16, 543, 90, 40, color.red, color.green, "main")
        quitbuttonfont = Font.settings(None, 30, "", color.white)
        # start button
        button("start", 694, 543, 90, 40, color.green, color.green, "start")
        startbuttonfont = Font.settings(None, 30, "", color.white)
        if self.buttonstwo:
            # make a button to select how many players you want
            # two players
            button_draw("two", 139, 267, 160, 65, color.grey, color.green, "two")
            twofont = Font.settings(Font.font_dynamix, 36, "2", color.black)
            screen.blit(twofont, (202, 263))
        if self.buttonsthree:
            # three players
            button_draw("three", 319, 267, 160, 65, color.grey, color.green, "three")
            threefont = Font.settings(Font.font_dynamix, 36, "3", color.black)
            screen.blit(threefont, (385, 263))
        if self.buttonsfour:
            # four players
            button_draw("four", 499, 267, 160, 65, color.grey, color.green, "four")
            fourfont = Font.settings(Font.font_dynamix, 36, "4", color.black)
            screen.blit(fourfont, (560, 263))

        # blit the button fonts
        screen.blit(quitbuttonfont, (425, 425))
        screen.blit(startbuttonfont, (225, 425))
        screen.blit(question, (10, 10))

    # if buttons are pressed, call this function so the self.players is adjusted accordingly
    def playerstwo(self):
        self.players = 2

    def playersthree(self):
        self.players = 3

    def playersfour(self):
        self.players = 4


class startgame:
    def __init__(self):
        # draw the buttons only if this value is true. its true by default,
        # set to false while moving
        # and set back to true once players are done moving.
        self.drawbuttons = True

    def draw(self, screen):
        # draw the game board
        # TODO insert correct image
        background = pygame.image.load("./Design_Afbeeldingen/playground.png")
        backgroundpos = (0, 0)
        background = background.convert()
        # blit background before drawing players so players get in front of background
        screen.blit(background, backgroundpos)
        if self.drawbuttons:
            # draw start and next button
            # start makes you go to a questionpage
            # nextplayer move the currentplayer to next player
            button("playground-main", 8, 8, 120, 40, color.red, color.green, "main")

            button("savegame", 8, 53, 120, 40, color.red, color.green, None)

            button("start", 83, 544, 135, 45, color.grey, color.green, "questions")

            button("Next", 583, 544, 135, 45, color.grey, color.green, "nextplayer")

        # draw whose turn it is currently
        if turns.currentplayer == player1:
            current = Font.settings(Font.font_skater, 37, "ITS'S " + turns.currentplayer.name + " TURN!", player1.color)
        elif turns.currentplayer == player2:
            current = Font.settings(Font.font_skater, 37, "ITS'S " + turns.currentplayer.name + " TURN!", player2.color)
        elif turns.currentplayer == player3:
            current = Font.settings(Font.font_skater, 37, "ITS'S " + turns.currentplayer.name + " TURN!", player3.color)
        elif turns.currentplayer == player4:
            current = Font.settings(Font.font_skater, 37, "ITS'S " + turns.currentplayer.name + " TURN!", player4.color)
        screen.blit(current, (250, 550))
        # draw the players
        player1.draw(screen)
        player2.draw(screen)
        if gamepage.players == 3:
            player3.draw(screen)
        elif gamepage.players == 4:
            player3.draw(screen)
            player4.draw(screen)


class turns:
    def __init__(self):
        # self.n True by default, set to false when a list is created, so the list_
        # _wont change every iteration.
        # currentplayer None, so we can put in a currentplayer once needed
        # list is empty, so we can insert the player data later on
        self.n = True
        self.currentplayer = None
        self.list = []

    def update(self):
        # this is ticked only once
        if self.n:
            # check for the amount of players, then put them in a list accordingly
            if gamepage.players == 4:
                self.list = {0: player1, 1: player2, 2: player3, 3: player4}
            elif gamepage.players == 3:
                self.list = {0: player1, 1: player2, 2: player3}
            else:
                self.list = {0: player1, 1: player2}
            # now shuffle the list to put the players in a random order
            random.shuffle(self.list)
            self.n = False
        # put the first player in the list
        if main.listremove < gamepage.players:
            self.currentplayer = self.list[main.listremove]

    def next(self):
        # ticked every time a player pressed nextplayer in the game
        # if the counter < the amount of players, add 1 to it so we move to next player
        if main.listremove < gamepage.players:
            main.listremove += 1
        # if counter == players, set it back to 0, we now start the player loop over again.
        if main.listremove == gamepage.players:
            main.listremove = 0
        # put the currentplayer in currentplayer(look in the list and use counter as key)
        self.currentplayer = self.list[main.listremove]


class whostarts:
    def __init__(self):
        # this is where we will give each player a category.
        # create a list with every category and set self.chosen to 0
        self.categories = ["Sports", "Geography", "Entertainment", "History"]
        self.chosen = 0

    def draw(self, screen):
        # drawing the background
        whostartsimg = pygame.image.load("./Design_Afbeeldingen/background.png")
        screen.blit(whostartsimg, (0, 0))
        # here we call turns.update, we now have a list with players
        turns.update()
        # draw continue button if all players chose a category
        # draw the category buttons only if the category is still in the list.
        # if the button is pressed remove the category from the list, put it in the player_
        # _ and stop showing the button for the corresponding category.
        if self.chosen >= gamepage.players:
            button_draw("Next", 692, 543, 90, 40, color.grey, color.green,"asknames")
            buttontext = Font.settings(Font.font_orange_juice, 36, "Next", color.black)
            screen.blit(buttontext, (700, 547))
        if "Sports" in self.categories:
            button_draw("Sports", 163, 207, 226, 83, color.grey, color.blue, "Sports")
            sports_text = Font.settings(Font.font_skater, 24, "SPORTS", color.blue)
            screen.blit(sports_text, (237, 240))
        if "Geography" in self.categories:
            button_draw("Geography", 410, 207, 226, 83, color.grey, color.green, "Geography")
            geography_text = Font.settings(Font.font_skater, 24, "GEOGRAPHY", color.green)
            screen.blit(geography_text, (468, 240))
        if "Entertainment" in self.categories:
            button_draw("Entertainment", 163, 311, 226, 83, color.grey, color.red, "Entertainment")
            entertainment_text = Font.settings(Font.font_skater, 24, "ENTERTAINMENT", color.red)
            screen.blit(entertainment_text, (199, 345))
        if "History" in self.categories:
            button_draw("History", 410, 311, 226, 83, color.grey, color.yellow, "History")
            history_text = Font.settings(Font.font_skater, 24, "HISTORY", color.yellow)
            screen.blit(history_text, (483, 345))
        # draw who starts
        starter = Font.settings(Font.font_skater, 45, str(turns.currentplayer.name) + "'S TURN!", color.black)
        screen.blit(starter, (252, 90))


class diceroll:
    def __init__(self):
        # self.n = 0 by default, it will be a randint between 1-6 when update is called
        # self.file = none, will be an image when update is called
        # self.x = 300, this is where the dice will be drawn by default
        # self.y = 100, this is where the dice will be drawn by default
        # self.showbuttons, to show buttons only if the dice is rolled.
        self.n = 0
        self.file = None
        self.x = 300
        self.y = 100
        self.showbuttons = False

    def update(self):
        # put in a randint, make self.file have corresponding img
        print(turns.currentplayer.correct)
        self.n = random.randint(1, 6)
        print(self.n)
        d1 = pygame.image.load("./Game_Dice/1.png")
        d2 = pygame.image.load("./Game_Dice/2.png")
        d3 = pygame.image.load("./Game_Dice/3.png")
        d4 = pygame.image.load("./Game_Dice/4.png")
        d5 = pygame.image.load("./Game_Dice/5.png")
        d6 = pygame.image.load("./Game_Dice/6.png")
        if self.n == 1:
            self.file = d1
        elif self.n == 2:
            self.file = d2
        elif self.n == 3:
            self.file = d3
        elif self.n == 4:
            self.file = d4
        elif self.n == 5:
            self.file = d5
        elif self.n == 6:
            self.file = d6

    def draw(self, screen):
        # draw the background
        diceroll_BG = pygame.image.load("./Design_Afbeeldingen/diceroll.png")
        screen.blit(diceroll_BG, (0, 0))
        # draw the dice
        button("roll dice", 620, 98, 102, 64, color.blue, color.green, "update")
        screen.blit(self.file, (self.x, self.y))
        # if diced, show the buttons
        if self.showbuttons:
            button("quit", 628, 523, 90, 40, color.white, color.green, "proceed")
            button("up", 628, 235, 90, 40, color.white, color.green, "up")
            button("down", 628, 325, 90, 40, color.white, color.green, "down")
            button("left", 580, 280, 90, 40, color.white, color.green, "left")
            button("right", 675, 280, 90, 40, color.white, color.green, "right")
    # pages for the dice animations
    # main.diced counts the amount of wall collisions.
    # if main.diced == 5 we set the page back to the dicepage, reset main.diced _
    # _ and show the buttons to move.
    # every time the dice hits a wall, it will move to another direction, according to
    # physics and stuff.

    def moverightup(self, screen):
        diceroll_move_BG = pygame.image.load("./Design_Afbeeldingen/diceroll.png")
        screen.blit(diceroll_move_BG, (0, 0))
        print(main.diced)
        if main.diced < 5:
            screen.blit(self.file, (self.x, self.y))
            self.y -= 4
            self.x += 4
            if self.x % 100 == 0:
                diceroll.update()
            time.sleep(0.001)
            if self.y < 40:
                page.pagenumber = 21
                main.diced += 1
            elif self.x > 419:
                page.pagenumber = 24
                main.diced += 1
        else:
            page.pagenumber = 12
            main.diced = 0
            self.showbuttons = True
        print(page.pagenumber)

    def moverightdown(self, screen):
        diceroll_move1_BG = pygame.image.load("./Design_Afbeeldingen/diceroll.png")
        screen.blit(diceroll_move1_BG, (0, 0))
        print(main.diced)
        if main.diced < 5:
            screen.blit(self.file, (self.x, self.y))
            self.y += 4
            self.x += 4
            if self.x % 100 == 0:
                diceroll.update()
            time.sleep(0.001)
            if self.y > 428:
                page.pagenumber = 22
                main.diced += 1
            elif self.x > 419:
                page.pagenumber = 23
                main.diced += 1
        else:
            page.pagenumber = 12
            main.diced = 0
            self.showbuttons = True
        print(page.pagenumber)

    def moveleftup(self, screen):
        diceroll_move2_BG = pygame.image.load("./Design_Afbeeldingen/diceroll.png")
        screen.blit(diceroll_move2_BG, (0, 0))
        print(main.diced)
        if main.diced < 5:
            screen.blit(self.file, (self.x, self.y))
            self.y -= 4
            self.x -= 4
            if self.x % 100 == 0:
                diceroll.update()
            time.sleep(0.001)
            if self.y < 40:
                page.pagenumber = 23
                main.diced += 1
            elif self.x < 30:
                page.pagenumber = 22
                main.diced += 1
        else:
            page.pagenumber = 12
            main.diced = 0
            self.showbuttons = True
        print(page.pagenumber)

    def moveleftdown(self, screen):
        diceroll_move3_BG = pygame.image.load("./Design_Afbeeldingen/diceroll.png")
        screen.blit(diceroll_move3_BG, (0, 0))
        print(main.diced)
        if main.diced < 5:
            screen.blit(self.file, (self.x, self.y))
            self.y += 4
            self.x -= 4
            if self.x % 100 == 0:
                diceroll.update()
            time.sleep(0.001)
            if self.y > 428:
                page.pagenumber = 24
                main.diced += 1
            elif self.x < 30:
                page.pagenumber = 21
                main.diced += 1
        else:
            page.pagenumber = 12
            main.diced = 0
            self.showbuttons = True
        print(page.pagenumber)


class diceroll2:
    def __init__(self):
        # this is the same as diceroll, minus the animation. the page is loaded and _
        # _ instantly updates, giving a random dicenumber, and prints this on the screen.
        # the user can click on a direction and he will move there.
        # the user is movingplayer. he is set in this value at the player.check()
        # where we check for a collision, and if collision, we put the collided with player
        # in this value, and call this page.
        self.n = 0
        self.file = None
        self.movingplayer = None

    def update(self):
        turns.currentplayer.correct = False
        print(turns.currentplayer.correct)
        diceroll.n = random.randint(1, 6)
        print(diceroll.n)
        d1 = pygame.image.load("./Game_Dice/1.png")
        d2 = pygame.image.load("./Game_Dice/2.png")
        d3 = pygame.image.load("./Game_Dice/3.png")
        d4 = pygame.image.load("./Game_Dice/4.png")
        d5 = pygame.image.load("./Game_Dice/5.png")
        d6 = pygame.image.load("./Game_Dice/6.png")
        if diceroll.n == 1:
            self.file = d1
        elif diceroll.n == 2:
            self.file = d2
        elif diceroll.n == 3:
            self.file = d3
        elif diceroll.n == 4:
            self.file = d4
        elif diceroll.n == 5:
            self.file = d5
        elif diceroll.n == 6:
            self.file = d6

    def draw(self, screen):
        diceroll_BG = pygame.image.load("./Design_Afbeeldingen/diceroll.png")
        screen.blit(diceroll_BG, (0, 0))

        dice = Font.settings(None, 50, self.movingplayer.name + " diced:", color.white)
        screen.blit(dice, (10, 10))
        screen.blit(self.file, (15, 60))
        button("quit", 628, 523, 90, 40, color.white, color.green, "proceed2")
        button("up", 628, 235, 90, 40, color.white, color.green, "up2")
        button("down", 628, 325, 90, 40, color.white, color.green, "down2")
        button("left", 580, 280, 90, 40, color.white, color.green, "left2")
        button("right", 675, 280, 90, 40, color.white, color.green, "right2")


class endscreen:
    def __init__(self):
        # we go to this page once a player reached to top.
        # his name and category are inserted
        self.winner_name = None
        self.winner_category = None

    def update(self):
        pass

    def draw(self, screen):
        endscreen_BG = pygame.image.load("./Design_Afbeeldingen/endscreen.png")
        screen.blit(endscreen_BG, (0, 0))

        # print the name and category and quit button
        self.winner_name = Font.settings(Font.font_doctor_light, 45, "Player: " + turns.currentplayer.name, turns.currentplayer.color)
        self.winner_category = Font.settings(Font.font_doctor_light, 45, "Category: " + turns.currentplayer.category, turns.currentplayer.color)

        button("quit", 16, 543, 90, 40, color.green, color.green, "quit")
        screen.blit(self.winner_name, (182, 217))
        screen.blit(self.winner_category, (182, 265))
        # add a point to the winning player
        data.player_win()


class settings:
    def __init__(self):
        pass

    def update(self):
        pass

    def draw(self, screen):
        background_img = pygame.image.load("./Design_Afbeeldingen/settings.png")
        # blit background before buttons
        screen.blit(background_img, (0, 0))
        # draw buttons
        # if pressed on test, make a sound if on, else dont
        button("soundon", 335, 201, 130, 60, color.grey, color.green, "sound0")
        button("soundoff", 335, 271, 130, 60, color.grey, color.red, "sound1")
        button("soundtest", 317, 361, 170, 112, color.grey, color.black, "soundtest")
        button("back", 16, 543, 90, 40, color.red, color.green, "main")

        string = Font.settings(None, 36, Input.string, color.black)
        screen.blit(string, (10, 10))


class names:
    def __init__(self):
        self.n = 0

    def update(self):
        pass

    def draw(self, screen):
        # you can type on when this page is displayed. this string you typed will be
        # printed immediately. if pressed on changeplayer button, the current string
        # is put into currentplayer.name. the string is set empty and the current
        # player is set to next player.
        #page background
        playername_BG = pygame.image.load("./Design_Afbeeldingen/playername1.png")
        screen.blit(playername_BG, (0, 0))
        button("back", 16, 543, 90, 40, color.red, color.green, "start")
        if self.n == gamepage.players:
            button("next", 692, 543, 90, 40, color.grey, color.green, "whostarts")
        button("Save player", 297, 336, 217, 38, color.blue, color.green, "changeplayer")
        string = Font.settings(None, 36, Input.string, color.black)
        screen.blit(string, (281, 286))
        currentPlayer_CY = Font.settings(Font.font_skater, 45, str(turns.currentplayer.name) + "'S TURN!", color.black)
        screen.blit(currentPlayer_CY, (252, 90))


# this is the main loop that updates and draws the entities
class main:
    # Set up screen
    width = 800
    height = 600
    size = (width, height)
    screen = pygame.display.set_mode(size)
    pygame.display.set_icon(pygame.image.load("./Design_Afbeeldingen/background.png"))
    pygame.display.set_caption("NulTien")
    # Stop the game loop(temporary)
    stop_loop = False
    # create a counter for turns
    listremove = 0
    # extra False, set to True if extra diceroll(if collision)
    # set back to false when the player has moved.
    extra = False
    # counter for dice animation
    diced = 0
    # set up tickrate
    clock = pygame.time.Clock()
    winner_name = None
    winner_category = None
    winner_color = None
    # If a player has won
    player_won = False
    # sound is on by default, you change this value in the settings page.
    sound = True
    # create a list of keys(one representing the state, the other what goes in the string if pressed)
    keylist = [(pygame.K_a, "a"), (pygame.K_b, "b"), (pygame.K_c, "c"), (pygame.K_d, "d"), (pygame.K_e, "e"),
               (pygame.K_f, "f"), (pygame.K_g, "g"), (pygame.K_h, "h"), (pygame.K_i, "i"), (pygame.K_j, "j"),
               (pygame.K_k, "k"), (pygame.K_l, "l"), (pygame.K_m, "m"), (pygame.K_n, "n"), (pygame.K_o, "o"),
               (pygame.K_p, "p"), (pygame.K_q, "q"), (pygame.K_r, "r"), (pygame.K_s, "s"), (pygame.K_t, "t"),
               (pygame.K_u, "u"), (pygame.K_v, "v"), (pygame.K_w, "w"), (pygame.K_x, "x"), (pygame.K_y, "y"),
               (pygame.K_z, "z")]
    lengthlist = len(keylist)

    def __init__(self, screen, size):
        self.size = size
        self.screen = screen

        # Start PyGame
        pygame.init()

        # Set up the default font
        self.font = pygame.font.Font(None, 30)

    # Draw everything
    def draw(self):
        # Clear the screen
        self.screen.fill((0, 0, 0))

        # Draw the scenes
        # if certain page number: draw corresponding page
        if page.pagenumber == 0:
            mainpage.draw(self.screen)
        elif page.pagenumber == 2:
            instructionpage.draw(self.screen)
        elif page.pagenumber == 4:
            questionpage.draw_category(self.screen)
        elif page.pagenumber == 5:
            main.clock.tick(15)
            questionpage.draw_sports(self.screen)
        elif page.pagenumber == 13:
            main.clock.tick(15)
            questionpage.draw_entertainment(self.screen)
        elif page.pagenumber == 14:
            main.clock.tick(15)
            questionpage.draw_history(self.screen)
        elif page.pagenumber == 15:
            main.clock.tick(15)
            questionpage.draw_geography(self.screen)
        # If answer is correct
        elif page.pagenumber == 6:
            questionpage.draw_rating(1, self.screen)
        # If answer is incorrect
        elif page.pagenumber == 7:
            questionpage.draw_rating(0, self.screen)
        elif page.pagenumber == 1:
            gamepage.draw(self.screen)
        elif page.pagenumber == 3:
            startgame.draw(self.screen)
        elif page.pagenumber == 11:
            whostarts.draw(self.screen)
        elif page.pagenumber == 12:
            diceroll.draw(self.screen)
        elif page.pagenumber == 16:
            diceroll2.draw(self.screen)
        elif page.pagenumber == 21:
            diceroll.moverightdown(self.screen)
        elif page.pagenumber == 22:
            diceroll.moverightup(self.screen)
        elif page.pagenumber == 23:
            diceroll.moveleftdown(self.screen)
        elif page.pagenumber == 24:
            diceroll.moveleftup(self.screen)
        elif page.pagenumber == 30:
            endscreen.draw(self.screen)
        elif page.pagenumber == 31:
            settings.draw(self.screen)
        elif page.pagenumber == 32:
            names.draw(self.screen)
        elif page.pagenumber == 40:
            score.draw(self.screen)
        # Flip the screen
        pygame.display.flip()
        if not page.pagenumber == 5:
            if not page.pagenumber == 13:
                if not page.pagenumber == 14:
                    if not page.pagenumber == 15:
                        main.clock.tick(60)

    # The game loop
    def game_loop(self):
        while not process_events():
            self.draw()


# this is a class to define when to quit the game
def process_events():
    for event in pygame.event.get():
        if event == pygame.QUIT:
            # give the signal to quit
            return True
        if page.pagenumber == 32:
            if event.type == pygame.KEYDOWN:
                for i in range(main.lengthlist):
                    if event.key == main.keylist[i][0]:
                        if Input.characters < 9:
                            Input.string += main.keylist[i][1]
                            Input.characters += 1
                if event.key == pygame.K_BACKSPACE:
                    Input.string = Input.string[:-1]
                    Input.characters -= 1
    return False


# this is where we make an instance of the main program and run the loop
def program():
    game = main(main.screen, main.size)
    game.game_loop()

Input = Input()
turns = turns()
questionpage = Questionpage(main.screen)
# make instances of classes
instructionpage = instructionpage()
mainpage = mainpage()
gamepage = gamepage()
diceroll = diceroll()
diceroll2 = diceroll2()
endscreen = endscreen()
settings = settings()
color = color()
startgame = startgame()
whostarts = whostarts()
names = names()
data = Database()
score = HighScore()
# create page and set default pagenumber
page = page(0)
# create the players
player1 = player("PLAYER1", color.blue, 15, 280, 0, 0, 1)
print(player1)
player2 = player("PLAYER2", color.yellow, 15, 321, 10, 0, 2)
print(player2)
player3 = player("PLAYER3", color.green, 15, 362, 20, 0, 3)
print(player3)
player4 = player("PLAYER4", color.red, 15, 401, 30, 0, 4)
print(player4)

program()
